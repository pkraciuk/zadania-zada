# -*- coding: utf-8 -*-

from cms.models.pluginmodel import CMSPlugin
from django.db import models


class Hello(CMSPlugin):
    send_button = models.BooleanField(u'Przycisk send')
    data_width = models.IntegerField(u'Szerokosc')
    data_faces = models.BooleanField(u'Twarze z facebooka')