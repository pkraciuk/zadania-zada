
Praca domowa #A
===============

Dokończyć stronę domową z zajęć oraz:

* Zintegrować aplikację do blogowania z pracy domowej #7 (dla wielu użytkowników).
  Menu powinno uwzględniać także wpisy na blogu i ich grupowanie po autorze i np. miesiącu.

* Stworzyć i zintegrować aplikację do dodwania komentarzy dla dowolnej strony.
  W razie potrzeby utworzyć odpowiednie wtyczki.

* Stronę kontaktu wzbogacić o formularz kontaktowy oparty o Django Forms. Strona powinna być
  w pełni zintegrowana z django-cms.
