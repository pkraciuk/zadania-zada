from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from django.utils.datetime_safe import datetime
from django.utils.translation import ugettext_lazy as _
from microblog.models import Entry
from microblog.models import EntryForm

from .models import Hello


class FacebookPlugin(CMSPluginBase):
    model = Hello
    name = _("Facebook plugin")
    render_template = "facebook_plugin.html"

    def render(self, context, instance, placeholder):
        context['instance'] = instance
        return context

class EntryListPlugin(CMSPluginBase):
    name = _("Entrylist plugin")
    render_template = "entrylist_plugin.html"
    def render(self,context,instance,placeholder):
        list = Entry.objects.all()
        context['list']=list
        return context

class EntryDetailsPlugin(CMSPluginBase):
    name = _("Entrydetails plugin")
    render_template = "entrydetails_plugin.html"

    def render(self, context, instance, placeholder):
        entry_id = '1' #___________-przekazac entry_id jako parametr_________________!!
        entry = Entry.objects.filter(id = entry_id)[0]
        context['entry']=entry
        return context

class AddEntryPlugin(CMSPluginBase):
    name=_("Addentry plugin")
    render_template = "addedentry_plugin.html"

    def render(self, context, instance, placeholder):

        #f = EntryForm(request.POST) #zczytaj dane z post'a
        #new_entry = f.save(commit=False)
        #new_entry.Time= datetime.now()
        #new_entry.Author = '' #zczytaj z post'a
        #new_entry.save()

        return context

plugin_pool.register_plugin(FacebookPlugin)
plugin_pool.register_plugin(EntryListPlugin)
plugin_pool.register_plugin(EntryDetailsPlugin)
plugin_pool.register_plugin(AddEntryPlugin)