from django.test import TestCase
from django.test.utils import setup_test_environment
from django.test.client import Client
from models import Entry


class MicroblogTest(TestCase):

    def test_connection(self):
        setup_test_environment()
        client = Client()
        response = client.get('http://localhost:8000/blog/')
        self.assertEqual(response.status_code,200)

    def test_response(self):
        setup_test_environment()
        client = Client()
        response = client.get('http://localhost:8000/blog/')
        self.assertContains(response,"Entries")

    def test_adding(self):
        b = Entry(Author='tester', Title='testowy_wpis', Text='test test test test test')
        b.save()
        setup_test_environment()
        client = Client()
        response = client.get('http://localhost:8000/blog/')
        self.assertContains(response,"tester")

    def test_receiving(self):
        setup_test_environment()
        client = Client()
        response = client.get('http://localhost:8000/blog/')
        self.assertNotContains(response,"nieistniejacy")

    def test_logging(self):
        setup_test_environment()
        client = Client()
        response = client.get('http://localhost:8000/blog/')
        self.assertNotContains(response,"Add entry")

    def test_unlogin_add(self):
        setup_test_environment()
        client = Client()
        response = client.get('http://localhost:8000/blog/add/')
        self.assertContains(response,"Log in")


