from django.utils.datetime_safe import datetime
from django import forms
from django.shortcuts import render
from microblog.models import Entry
from microblog.models import EntryForm
from microblog.models import TagForm
from microblog.models import Tag
from microblog.models import RegisterForm
from microblog.models import Register
from microblog.models import EditForm
from datetime import timedelta

class LoginForm(forms.Form):
    username = forms.CharField(max_length=50)
    password = forms.CharField(max_length=50)


def index(request):
    logged = False
    user = ''
    try:
        username = request.session['loginabc']
        request.session['logInToSite']
        user = Register.objects.get(username=username)
        logged = True
    except:
        logged = False
    entry_list = Entry.objects.all()
    for entry in entry_list:
        #print "teraz: ",datetime.now()-timedelta(hours=2,minutes=30)
        #print "wtedy: ",entry.Time.replace(tzinfo=None)
        if entry.Time.replace(tzinfo=None)>datetime.now()-timedelta(hours = 2,minutes=30):
            entry.changeable=True
        else:
            entry.changeable = False
        edited = False
        print entry.edittime
        if entry.edittime.replace(tzinfo=None)>datetime.now().replace(year=1971):
            edited = True

    tags = Tag.objects.all()
    return render(request, 'microblog/index.html', {'logged': logged, 'list': entry_list,'tags':tags,'admin':'admin','user':user,'moderator':'moderator'})


def details(request,entry_id):
    list = Entry.objects.filter(id = entry_id)
    user = ''
    try:
        logged = request.session['logInToSite']
        #list = Entry.objects.filter(Author__exact = request.session['loginabc'])

        user = Register.objects.filter(username = request.session['loginabc'])[0]
    except:
        logged = False
        user = 'Unknown'
    tags_all = Tag.objects.all()
    tags =Tag.objects.filter(entries=list[0])
    return render(request,'microblog/details_template.html',{'entry':list[0],'entry_id':entry_id,'tags_all':tags_all,'tags':tags,'user':user,'logged':logged})


def login(request):
    if request.method == 'POST':
        error = 'Username or password incorrect'
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            if Register.objects.filter(username = username) or (username=='a' and password == 'a'):
                #tworzenie ciasteczka
                request.session['loginabc'] = username
                request.session['logInToSite'] = True
                return render(request,'microblog/logged.html',{'username':username})
    else:
        form = LoginForm()
        error = None
    return render(request,'microblog/login.html',{'form':form,'error':error})


def add(request):
    if request.method == 'POST':
        form = EntryForm(request.POST)
        if form.is_valid():
            f = EntryForm(request.POST)
            new_entry = f.save(commit=False)
            new_entry.Time= datetime.now()
            try:
                new_entry.Author = request.session['loginabc']
            except:
                new_entry.Author = 'Unknown'
            #dodaj czas i autora
            new_entry.save()
            return render(request,'microblog/added.html',{'title':new_entry.Title})
    else:
        form = EntryForm()
    return render(request,'microblog/add.html',{'form': form,})

def register(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            f = RegisterForm(request.POST)
            new_entry = f.save(commit=False)
            new_entry.status = 'regular'
            new_entry.save()
            return render(request,'microblog/registered.html')
    else:
        form = RegisterForm()
    return render(request,'microblog/register.html',{'form':form,})

def edit(request,entry_id):
    entry = Entry.objects.get(id = entry_id)
    title = entry.Title
    if request.method == 'POST':
        form = EditForm(request.POST)
        if form.is_valid():
            f = EditForm(request.POST)
            new_entry = f.save(commit=False)
            new_entry.Time = entry.Time
            new_entry.Author = entry.Author
            new_entry.Title = entry.Title
            new_entry.edittime = datetime.now()
            new_entry.id = entry.id
            new_entry.edited = True
            try:
                new_entry.editauthor = request.session['loginabc']
            except:
                new_entry.editauthor = 'Unknown'
            new_entry.save()
            return render(request,'microblog/edited.html',{'title':title})

    form = EditForm()
    return render(request,'microblog/edit.html',{'form':form,'title':title})




def tag(request):
    if request.method == 'POST':
        form = TagForm(request.POST)
        if form.is_valid():
            f = TagForm(request.POST)
            new_tag = f.save()
            new_tag.save()
            return render(request,'microblog/added.html',{'title':new_tag.title})
    form = TagForm()
    return render(request,'microblog/tag.html',{'form':form,})


def tagged(request,entry_id,tag_id):
    tag = Tag.objects.get(id = tag_id)
    entry = Entry.objects.get(id = entry_id)
    tag.entries.add(entry)
    tag.save()
    return render(request,'microblog/tagged.html',{'tag':tag,'entry':entry})

def logout(request):
    try:
        del request.session['loginabc']
        request.session['logInToSite'] = False
    except KeyError:
        pass
    return render(request, 'microblog/logout.html')



def tagsearch(request,tag_id):
    tag = Tag.objects.get(id = tag_id)
    list = Entry.objects.filter(tag = tag_id)
    return render(request,'microblog/tagsearch.html',{'list':list})



def Error(request):
    return render(request, 'microblog/login.html')

def user_entries(request):
    try:
        logged = request.session['logInToSite']
        list = Entry.objects.filter(Author__exact = request.session['loginabc'])
        return render(request, 'microblog/user_entries.html', {'logged': logged, 'list': list})
    except KeyError:
        list = Entry.objects.all()
        return render(request, 'microblog/login.html')

def month(request,month_id):
    list = Entry.objects.filter(Time__month = month_id)
    return render(request, 'microblog/user_entries.html', {'logged': True, 'list': list})
