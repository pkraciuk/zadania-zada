from django.core.validators import RegexValidator
from django.db import models
from django.forms import ModelForm


class Entry(models.Model):
    Author = models.CharField(max_length=50)
    Title = models.CharField(max_length=50)
    Text = models.CharField(max_length=200,validators=[RegexValidator(regex='^.{10}', message='Length has to be at least 10', code='nomatch')])
    Time = models.DateTimeField()
    editauthor = models.CharField(max_length=50)
    edittime = models.DateTimeField(default='1970-01-01')
    edited = models.NullBooleanField()
    changeable = models.NullBooleanField()

class Tag(models.Model):
    title = models.CharField(max_length = 30)
    entries = models.ManyToManyField(Entry)
    def __unicode__(self):
        return self.title

class Register(models.Model):
    username = models.CharField(max_length=30)
    password = models.CharField(max_length=30)
    status = models.CharField(max_length = 10)
    email = models.CharField(max_length = 100,validators = [RegexValidator(regex='.+@.+\\.[a-z]+',message = 'Invalid e-mail address')])

class RegisterForm(ModelForm):
    class Meta:
        model = Register
        exclude = ('status')

class EntryForm(ModelForm):
    class Meta:
        model = Entry
        exclude = ('Time','Author','changeable','editauthor','edittime','edited')

class EditForm(ModelForm):
    class Meta:
        model = Entry
        exclude = ('Time','Author','changeable','editauthor','editable','edittime','Title','edited')

class TagForm(ModelForm):
    class Meta:
        model = Tag
        exclude = ('entries')