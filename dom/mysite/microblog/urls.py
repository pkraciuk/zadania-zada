from django.conf.urls import patterns, url
from django.views.generic import TemplateView
import views

urlpatterns = patterns('',
    url(r'^$', views.index,name='index'),

    url(r'^login/$',views.login,name='login'),

    url(r'^add/$',views.add,name='add'),

    url(r'^logout/$',views.logout,name='logout'),

    url(r'^user_entries/$',views.user_entries,name='user_entries'),

    url(r'^entry/(?P<entry_id>\d+)/tag/(?P<tag_id>\d+)/$',views.tagged,name='tagged'),

    url(r'^entry/(?P<entry_id>\d+)/$',views.details,name='details'),

    url(r'^entry/$',views.tag,name='tag'),

    url(r'^tag/(?P<tag_id>\d+)/$',views.tagsearch,name = 'tagsearch'),

    url(r'^register/$',views.register, name = 'register'),

    url(r'^edit/(?P<entry_id>\d+)/$',views.edit,name='edit'),

    url(r'^error/$',views.Error,name='error'),

    url(r'^month/(?P<month_id>\d+)/$',views.month,name='month'),
)







































































































